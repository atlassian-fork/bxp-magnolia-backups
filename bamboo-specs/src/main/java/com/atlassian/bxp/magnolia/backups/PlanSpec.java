package com.atlassian.bxp.magnolia.backups;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.pbc.ContainerSize;
import com.atlassian.bamboo.specs.api.builders.pbc.ExtraContainer;
import com.atlassian.bamboo.specs.api.builders.pbc.ExtraContainerSize;
import com.atlassian.bamboo.specs.api.builders.pbc.PerBuildContainerForJob;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
//import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchIntegration;
//import com.atlassian.bamboo.specs.api.builders.plan.PlanBranchIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.builders.notification.PlanCompletedNotification;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledTrigger;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;


/**
 * Class to publish Bamboo plans to run the Magnolia App and Repositories Backups.
 * <p>
 * Before this plan spec can work, create a bamboo linked repo called "BXP Magnolia Backups" to
 * the bitbucket cloud repo called bxp-magnolia-backups for bamboo to scan for the plan spec.
 * Edit the repository, in the Bamboo Specs tab, turn on the "Scan for Bamboo Specs".
 * <p>
 * Edit the project's "Bamboo Specs repositories".
 * For example, "BXP Team Bamboo"/"Bamboo Specs repositories".
 * Add "BXP Magnolia Backups" as one of the "Bamboo Specs repositories" to this project.
 * </p>
 * <p>
 * Use the @BambooSpec annotation to help Bamboo to find the code and execute main().
 * <p>
 * The plan task is to run the snapshot-docker-image.sh script lives in the linked repo.
 * <p>
 * Bamboo plan spec does not have APIs to create and configure plan branches.
 * We work around this by creating individual build plan for each backup.
 * Each plan triggers the backups for Author, Proof, and Truth environment individually.
 * See the enum Branch for future support.
 * <p>
 * Each plan sends notification messages to the Slack #bxp-deployment channel.
 * <p>
 * Internal classes are used to encapsulate the topology of project, plan, stage(s),
 * job(s) and task(s). We use the builder pattern.
 * <p>
 *
 * @author ltang
 */

@BambooSpec
public class PlanSpec {
    // TBD: Use a properties file for the String contents.
    // This way we may not need to change the code.
    // But it may be more difficult to debug changes.

    private final static String OWNER_NAME = "cgati";

    // See the group names in Bamboo/Administration/Security/Groups:
    private final static String DEV_GROUPS[] = {"bxp-devs-dl-devs"};
    private final static String ADMIN_GROUPS[] = {"bxp-devs-dl-admins"};

    // Test with the sandbox server. Use the push-request branch of the linked repo:
    private final static String BAMBOO_SANDBOX_SERVER_URL = "https://sandbox-bamboo.internal.atlassian.com";
    // Real build on the engineering server. Use the master branch of the linked repo:
    private final static String BAMBOO_ENG_SERVER_URL = "https://engservices-bamboo.internal.atlassian.com";
    private final static String BAMBOO_SERVER_URL = BAMBOO_ENG_SERVER_URL; // BAMBOO_SANDBOX_SERVER_URL;

    // Use the following maps to capture variables and values for the plan and branches:
    private final static Map<String, String> planVariableMap = new HashMap<String, String>() {{
        // We do not do the docker login with this:
        // put("micros_password", "BAMSCRT@0@0@guh4DiFR8NB07/DTDsCqfDaXMD0A0RvdqTwnaI2NY20="); // <-- DEBUG
        // put("micros_user_id", "webops-auto-bot");
    }};
    private final static Map<String, String> authVariableMap = new HashMap<String, String>() {{
        putAll(planVariableMap);
        put("repository_name", "magnolia-repository-prod/author");
        put("target_node", "author.marketing.internal.atlassian.com");
    }};
    private final static Map<String, String> proofVariableMap = new HashMap<String, String>() {{
        putAll(planVariableMap);
        put("repository_name", "magnolia-repository-prod/proof");
        put("target_node", "proof.marketing.internal.atlassian.com");

    }};
    private final static Map<String, String> truthVariableMap = new HashMap<String, String>() {{
        putAll(planVariableMap);
        put("repository_name", "magnolia-repository-prod/public");
        put("target_node", "truth.marketing.internal.atlassian.com");

    }};

    // Run author backup daily at 07:00:00. Other backup are one hour apart.
    // Warning: the bamboo server may not be in the same time zone we wanted. Make no assumption.
    private final static String TIME_ZONE_NAME = "America/Los_Angeles";
    private final static MySchedule DAILY_BACKUP_TIME =
            new MySchedule(TIME_ZONE_NAME, 2, 0, 0); // <-- DEBUG
    public final static int SCHEDULE_MINUTES_APART = 30;

    /**
     * Capture schedule time with the time zone adjustment.
     */
    public static class MySchedule {
        private final String userZoneIdName; // user specified time zone (TZ)
        private LocalTime userLocalTime; // user specified hours/minutes/seconds
        private String sysZoneIdName; // The system default time zone
        private final LocalTime sysLocalTime; // The system hours/minutes/seconds adjusted for user TZ
        private final DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss");

        public MySchedule(String userZoneIdName, int hr, int min, int sec) {
            this.userZoneIdName = userZoneIdName;
            this.sysZoneIdName = ZoneId.systemDefault().getId();
            this.userLocalTime = LocalTime.of(hr, min, sec);
            if (this.userZoneIdName.equals(this.sysZoneIdName)) {
                this.sysLocalTime = this.userLocalTime;
            } else {
                this.sysLocalTime = this.userLocalTime.minusSeconds(
                        LocalDateTime.now().atZone(ZoneId.of(userZoneIdName)).getOffset().getTotalSeconds() -
                                LocalDateTime.now().atZone(ZoneId.systemDefault()).getOffset().getTotalSeconds());
            }
        }

        public MySchedule(String zoneIdName, LocalTime localTime) {
            this(zoneIdName, localTime.getHour(), localTime.getMinute(), localTime.getSecond());
        }

        @Override
        public String toString() {
            if (this.userZoneIdName.equals(this.sysZoneIdName)) {
                return sysLocalTime.format(format) + " " + sysZoneIdName;
            }
            return "System " + sysLocalTime.format(format) + " " + sysZoneIdName +
                    " is user specified " + userLocalTime.format(format) + " " + userZoneIdName;
        }

        public MySchedule plusHours(int hr) {
            return new MySchedule(userZoneIdName, userLocalTime.plusHours(hr));
        }

        public MySchedule plusMinutes(int min) {
            return new MySchedule(userZoneIdName, userLocalTime.plusMinutes(min));
        }

        public LocalTime getTime() {
            return sysLocalTime;
        }

        @Override
        public boolean equals(Object object) {
            if (object instanceof MySchedule) {
                MySchedule other = (MySchedule) object;
                return sysLocalTime.equals(other.sysLocalTime) &&
                        userZoneIdName.equals(other.userZoneIdName);
            }
            return false;
        }
    }

    /**
     * Capture the essential for each plan branch or backup plan.
     */
    public static enum Branch {
        AUTHOR(DAILY_BACKUP_TIME, authVariableMap),
        TRUTH(DAILY_BACKUP_TIME.plusMinutes(SCHEDULE_MINUTES_APART), truthVariableMap),
        PROOF(DAILY_BACKUP_TIME.plusMinutes(SCHEDULE_MINUTES_APART*2), proofVariableMap);

        private final static String PLAN_NAME_FORMAT = "MAGNOLIABACKUP%s";
        private final static String PLAN_LONG_NAME_FORMAT = "BXP Magnolia Backup for %s";
        private final static String PLAN_DESCRIPTION_FORMAT = "Build plan for %s Magnolia backup";

        private final MySchedule schedule;
        private final String scheduleDescription;
        private final Map<String, String> variables;
        private final String planKey;
        private final String planName;
        private final String planDescription;

        Branch(MySchedule schedule, Map<String, String> variables) {
            this.schedule = schedule;
            this.variables = variables;
            this.planKey = String.format(PLAN_NAME_FORMAT, name());
            final String str = toString();
            this.planName = String.format(PLAN_LONG_NAME_FORMAT, str);
            this.planDescription = String.format(PLAN_DESCRIPTION_FORMAT, str);
            this.scheduleDescription = "Run every day at " + this.schedule;
        }

        @Override
        public String toString() {
            String name = name();
            return name.substring(0,1) + name.substring(1).toLowerCase();
        }

        public MySchedule getSchedule() {
            return schedule;
        }
    }

    /**
     * Encapsulate a Project.
     */
    private static class myProject {
        private final Project project;

        private myProject(final String key, final String name) {
            project = new Project()
                    .key(new BambooKey(key))
                    .name(name);
        }

        private static Project build(final String key, final String name) {
            return new myProject(key, name).project;
        }
    }

    /**
     * Encapsulate a Plan.
     */
    private static class MyPlan {
        // The plan is identified as <Key>-<Name>
        private final static String PLAN_KEY = "BXP";
        private final static String LINKED_REPOSITORY_NAME = "BXP Magnolia Backups"; // bxp-magnolia-backups
        private final static String PROJECT_NAME = "BXP Team Bamboo Project";
        private final static String SLACK_CHANNEL = "#bxp-deployments"; // "#lin-deployments"; <-- DEBUG

        private final static Variable PLAN_VARIABLES[] = new Variable[]{
                // See also branch.variables
        };

        final private Plan plan;
        final private Branch branch;

        private MyPlan(final Project project, final Branch branch) {
            this.plan = new Plan(project, branch.planName, new BambooKey(branch.planKey))
                    .description(branch.planDescription);
            this.branch = branch;
        }

        private static Plan build(final Branch branch) {
            // more easier to read than a giant statement of lot of nested lines.
            return new MyPlan(
                    myProject.build(PLAN_KEY, PROJECT_NAME), branch)
                    .setPluginConfig()
                    .setStages()
                    .setRepository()
                    .setTriggers(branch)
                    .setVariables()
                    .setBranchManagement()
                    .setNotifications()
                    .plan;
        }

        private MyPlan setPluginConfig() {
            plan.pluginConfigurations(
                    new ConcurrentBuilds(),
                    new AllOtherPluginsConfiguration().configuration(
                            new MapBuilder<String, Object>()
                                    .put("custom.planownership.bamboo.plugin.plan.config.ownerOfBuild", OWNER_NAME)
                                    .build()
                    )
            );
            return this;
        }

        private MyPlan setTriggers(final Branch branch) {
            plan.triggers(
                    // IMPORTANT: the poll trigger is for debug only!!!
                    // Please remove for production.
                    // Should trigger on merge to repository; pull every minute.
                    /*
                    new RepositoryPollingTrigger()
                            .pollEvery(1, TimeUnit.MINUTES),
                            */
                    // Should trigger on a schedule.
                    new ScheduledTrigger()
                            .scheduleOnceDaily(branch.getSchedule().getTime())
                            .description(branch.scheduleDescription)
            );
            return this;
        }

        private MyPlan setVariables() {
            plan.variables(PLAN_VARIABLES);
            return this;
        }

        private MyPlan setRepository() {
            /*
             * Tips:
             * (1) Create a new linked repository: go to The Bamboo Create dropdown to create a linked repository to the
             *     bitbucket cloud repository.
             * (2) Verify an existing linked repository: go to the bamboo Repositories tab find the linked repository name.
             * (3) Edit and test the connection.
             * (4) Give the repository access to a project's plan: go to the Project, for example BXP Team Bamboo Project.
             *     Select "Project Settings"/"Bamboo Specs repositories".
             *     From a list of available linked repository, find the linked repository and add it.
             * (5) Scan for the Bamboo Specs. Edit the linked repository/"Bamboo Specs" to turn on the
             *     "Scan for Bamboo Specs".
             *     Click on the "Scan" button". See the log for any errors.
             */
            plan.linkedRepositories(LINKED_REPOSITORY_NAME);
            return this;
        }

        private MyPlan setNotifications() {
            plan.notifications(new Notification()
                .type(new PlanCompletedNotification())
                .recipients(new AnyNotificationRecipient(
                    new AtlassianModule("com.atlassian.bamboo.plugins.bamboo-slack:recipient.slack"))
                    .recipientString("${bamboo.atlassian.slack.webhook.url.password}|" + SLACK_CHANNEL + "||")
                )
            );
            return this;
        }

        private MyPlan setBranchManagement() {
            plan.planBranchManagement(
               new PlanBranchManagement()
                   .delete(new BranchCleanup())
               .notificationForCommitters()
            );
            return this;
        }

        private MyPlan setStages() {
            plan.stages(MyStage.build(branch));
            return this;
        }

        private static PlanIdentifier getPlanIdentifier(final Branch branch) {
            return new PlanIdentifier(PLAN_KEY, branch.planKey);
        }
    }

    /**
     * Encapsulate a Task.
     */
    private static class MyTask {
        private static final String DESCRIPTION = "Set env vars and run build script";

        private final ScriptTask task;
        private final Branch branch;

        private MyTask(final String description, final Branch branch) {
            this.branch = branch;
            this.task = new ScriptTask().description(description);
        }
        /*
         * DOCKER_TLS_VERIFY assumes files exist in the following locations:
         * ${HOME}/.docker/ca.pem
         * ${HOME}/.docker/cert.pem
         * ${HOME}/.docker/key.pem
         * Bamboo docker task only have run and push; not create and tag!!
         *
         * IMPORTANT: If we checked in those files to a VCS, how clone to the build agent?
         */
        private MyTask setBuildScriptCommand() {
            task.interpreter(ScriptTaskProperties.Interpreter.SHELL)
                    // script lives in the VCS checkout directory
                    .fileFromPath("snapshot-docker-image.sh")
                    .argument(String.join(" ", new String[]{
                                    branch.variables.get("repository_name"),
                                    branch.variables.get("target_node")
                            })
                    );
            return this;
        }

        private static Task<?, ?>[] build(final Branch branch) {
            return new Task<?, ?>[] {
                    new VcsCheckoutTask().addCheckoutOfDefaultRepository(),
                    new MyTask(DESCRIPTION, branch).setBuildScriptCommand().task};
        }
    }

    /**
     * Encapsulate a Job.
     */
    private static class MyJob {
        private static final String DEFAULT_JOB = "Default Job";
        private static final String JOB_KEY = "JOB1";
        private static final String DOCKER_IMAGE = "docker.atl-paas.net/sox/bxp/player-piano:latest";

        private final Job job;
        private final Branch branch;

        private MyJob(final String name, final String key, final Branch branch) {
            this.branch = branch;
            this.job = new Job(name, new BambooKey(key));
        }

        private MyJob setPluginConfigurations() {
            // Run the job in the player piano docker container.
            // The job task will involve the shell script there.
            // All docker commands in the scrip task will work with docker.atl-paas.net.
            PerBuildContainerForJob playerPianoForJob = new PerBuildContainerForJob()
                    .image(DOCKER_IMAGE)
                    .size(ContainerSize.REGULAR)
                    .extraContainers(new ExtraContainer()
                            .name("docker")
                            .image("docker:stable-dind")
                            .size(ExtraContainerSize.REGULAR));
            job.pluginConfigurations(playerPianoForJob /*, MyJobCustomConfig.build()*/);  // <---- DEBUG
            return this;
        }

        private MyJob setTasks() {
            job.tasks(MyTask.build(branch));
            return this;
        }

        private MyJob setDockerConfig() {
            job.dockerConfiguration(new DockerConfiguration()
                    .enabled(false)); // Tasks will be executed in the native operating system of the Bamboo agent.
            return this;
        }

        private static Job build(final Branch branch) {
            return new MyJob(DEFAULT_JOB, JOB_KEY, branch)
                    .setPluginConfigurations()
                    .setTasks()
                    .setDockerConfig()
                    .job;
        }
    }

    /**
     * ncapsulate a custom job configuration.
     */
    private static class MyJobCustomConfig {
        private static final String cleanupCommands[] = new String[]{
                "sudo /usr/local/bin/docker_stop_all",
                "sudo /usr/local/bin/delete_lastUpdated_in_m2",
                "sudo /usr/local/bin/restore_m2_repository_permission"
        };
        private static final String processNames[] = new String[]{
                "vagrant plugin install",
                "multilog",
                "C:\\\\Windows",
                "XenDpriv.exe",
                "taskhost.exe"

        };
        private static final String filesToBeDeleted[] = new String[]{
                "~/.m2/repository/repository.xml",
                "~/.config/pip/pip.conf",
                "~/.bower/tmp",
                "~/.local"
        };

        private static final Map<String, String> deleteConfigMap = new TreeMap<String, String>() {{
            put("stable", "true");
            put("stable.free", "25.6"); // Is this a version number??
            put("stable.age", "60.0");
            put("workingdir", "true");
            put("files", String.join(",", filesToBeDeleted));
            put("snapshot", "true");
        }};

        private static final Map<String, Object> customConfigMap = new TreeMap<String, Object>() {{
            put("fail", "false");
            put("processes", "^(?!.*(" + String.join("|", processNames) + ")).*$");
            put("keep.workingdir.activebranches", "false");
            put("delete", deleteConfigMap);
            put("skipDocker", "true");
            put("commands", String.join(";", cleanupCommands));
            put("agents", ".*");
        }};

        private static AllOtherPluginsConfiguration build() {
            return new AllOtherPluginsConfiguration()
                    .configuration(new MapBuilder()
                            .put("custom", customConfigMap).build());
        }
    }

    /**
     * Encapsulate a Stage.
     */
    private static class MyStage {
        private final static String DEFAULT_STAGE = "Default Stage";
        private final Stage stage;
        private final Branch branch;

        private MyStage(final String description, final Branch branch) {
            this.branch = branch;
            this.stage = new Stage(description);
        }

        private MyStage setJobs() {
            stage.jobs(MyJob.build(branch));
            return this;
        }

        private static Stage build(final Branch branch) {
            return new MyStage(DEFAULT_STAGE, branch)
                    .setJobs()
                    .stage;
        }
    }

    /**
     *  Encapsulate a Plan Permission.
     */
    private static class MyPlanPermission {
        private final PlanPermissions planPermission;

        private MyPlanPermission(final PlanIdentifier id) {
            planPermission = new PlanPermissions(id);
        }

        private MyPlanPermission setPermission() {
            Permissions permissions = new Permissions();
            Arrays.asList(ADMIN_GROUPS).forEach(group ->
                    permissions.groupPermissions(group, PermissionType.ADMIN, PermissionType.CLONE, PermissionType.BUILD,
                            PermissionType.EDIT, PermissionType.VIEW));
            Arrays.asList(DEV_GROUPS).forEach(group ->
                    permissions.groupPermissions(group, PermissionType.BUILD, PermissionType.VIEW));
            planPermission.permissions(permissions
                    .loggedInUserPermissions(PermissionType.VIEW)
                    .anonymousUserPermissionView());
            return this;
        }

        private static PlanPermissions build(final Branch branch) {
            return new MyPlanPermission(MyPlan.getPlanIdentifier(branch))
                    .setPermission()
                    .planPermission;
        }
    }


    /**
     * Used for Junit test to validate the Bamboo custom configurations for job.pluginConfigurations().
     * @return the custom delete/stable configuration value.
     */
    public static String getDeleteConfStable() {
        Object o = MyJobCustomConfig.customConfigMap.get("delete");
        if (o instanceof Map) {
            Map m = (Map) o;
            o = m.get("stable");
            if (o instanceof String) return (String) o;
        }

        return "";
    }

    /**
     *  Create a plan. Used for junit test as well.
     * @param branch a Branch enumeration.
     * @return Plan.
     */
    public static Plan createPlan(final Branch branch) {
        return MyPlan.build(branch)
                /* Unfortunately, we cannot use the following to create plan branches
                .planBranchManagement(new PlanBranchManagement()
                        .branchIntegration(new BranchIntegration()
                                .integrationBranch(new PlanBranchIdentifier(new BambooKey(branch.name()))
                                )
                        )
                        .createForVcsBranchMatching(branch.name())
                        .createManually()
                        .notificationLikeParentPlan()
                        .triggerBuildsLikeParentPlan()
                        .triggerBuildsManually()
                )
                */
                ;
    }

    /**
     * Main function to define and publish a Plan.
     * The Main function is intended to be invoked by Bamboo only.
     *
     * @param args String[] not used.
     *             Function signature for bamboo to run the plan spec code.
     */
    public static void main(String[] args) {
        BambooServer bambooServer = new BambooServer(BAMBOO_SERVER_URL);
        Arrays.asList(Branch.values()).forEach(branch -> {
            bambooServer.publish(createPlan(branch));
            bambooServer.publish(MyPlanPermission.build(branch));
        });

    }
}
