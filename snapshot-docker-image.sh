#!/usr/bin/env bash

## This script is used to backup a Magnolia app environment
## use it to prepare a backup docker image,
## and use the backup timestamp as the docker image version.

USAGE="Usage: ./snapshot-docker-image.sh <source repository> <target docker node>"
## Author example: ./snapshot-docker-image.sh magnolia-repository-prod/author author.marketing.internal.atlassian.com
## Proof example:  ./snapshot-docker-image.sh magnolia-repository-prod/proof 10.125.129.61
## Public example: ./snapshot-docker-image.sh magnolia-repository-prod/public truth.marketing.internal.atlassian.com

MAGNOLIA_REPOSITORY_NAME="$1"
MAGNOLIA_TARGET_NODE="$2"

if [[ -z "$MAGNOLIA_REPOSITORY_NAME" ]] || [[ -z "$MAGNOLIA_TARGET_NODE" ]]; then
   echo $USAGE
   exit -1
fi

# The script exits immediately if a command exits with a non-zero status.
# The script treats unset variables as an error when substituting.
set -eu
# The script generates traces that start with '+'
# set -eux

DOCKER_BACKUP_DIR=${HOME}/.docker-$(date +%Y-%m-%d-%H-%M-%S)
WORKING_TEMP_FOLDER=$(mktemp -d)

STEP="7"
SRC_CONTAINER="magnolia-app"
SRC_REPO_CONTAINER="magnolia-repository"
BACKUP_REPO_CONTAINER="${SRC_REPO_CONTAINER}_backup"

# POSTFIX should be "" after testing
POSTFIX=""

ABS_MAGNOLIA_REPOSITORY_NAME="docker.atl-paas.net/bxp/${MAGNOLIA_REPOSITORY_NAME}"
## ABS_MAGNOLIA_REPOSITORY_NAME="docker.atl-paas.net/atlassianwebteam/$MAGNOLIA_REPOSITORY_NAME"
ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME="${ABS_MAGNOLIA_REPOSITORY_NAME}:$(date +%Y-%m-%d)${POSTFIX}"
ABS_LATEST_MAGNOLIA_REPOSITORY_NAME="${ABS_MAGNOLIA_REPOSITORY_NAME}:latest${POSTFIX}"

# Alpine Linux package manager to install packages before use.
apk add --no-cache jq gettext bash-completion

#
# Get the AWS credential according to a given role.
#
function assume_role {
    backup_role
    echo "Assuming role: $1"
    ASSUMED_ROLE=$1
    ROLE_CREDENTIALS=$(aws sts assume-role \
        --role-arn ${ASSUMED_ROLE} \
        --role-session-name bxp-nginx-entrypoint)
    export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-us-east-1}
    export AWS_ACCESS_KEY_ID="$(echo ${ROLE_CREDENTIALS} | jq -r .Credentials.AccessKeyId)"
    export AWS_SECRET_ACCESS_KEY="$(echo ${ROLE_CREDENTIALS} | jq -r .Credentials.SecretAccessKey)"
    export AWS_SECURITY_TOKEN="$(echo ${ROLE_CREDENTIALS} | jq -r .Credentials.SessionToken)"
}

function backup_role {
    # Back up environment variables pertaining to AWS
    CURRENT_ROLE=$(aws sts get-caller-identity)
    echo "Backing up current role: ${CURRENT_ROLE}"
    env | grep "AWS_" > /tmp/restore-aws-env.sh
    cat /tmp/restore-aws-env.sh | cut -d"=" -f1 | xargs echo "export" >> /tmp/restore-aws-env.sh
}

function restore_role {
    # Unset assumed role variables, restore micros set variables
    echo "Restoring original role"
    while read var; do unset $var; done < <(env | grep -i AWS | cut -d"=" -f1)
    source /tmp/restore-aws-env.sh
}

#
# Get the necessary docker certificate files for use by the docker commands.
# The AWS credential is required for the cred-stash command to work.
# So call the assume_role function before this.
#
assume_role "arn:aws:iam::386531163347:role/CredstashRead"

function copy_docker_pem {

    if [ -d "${HOME}/.docker" ]; then
      if [ -f "${HOME}/.docker/config.json" ]; then
        echo "DEBUG>> Use ${HOME}/.docker/config.json $(cat ${HOME}/.docker/config.json)"
      fi
      mv ${HOME}/.docker ${DOCKER_BACKUP_DIR}
    fi

    mkdir -pv ${HOME}/.docker
    credstash get docker-ca-cert | base64 -d > ${HOME}/.docker/ca.pem
    credstash get docker-client-cert | base64 -d > ${HOME}/.docker/cert.pem
    credstash get docker-client-key | base64 -d > ${HOME}/.docker/key.pem

    if [ -d "${DOCKER_BACKUP_DIR}" ]; then
      if [ -f "${DOCKER_BACKUP_DIR}/config.json" ]; then
        cp ${DOCKER_BACKUP_DIR}/config.json ${HOME}/.docker/config.json
        echo "DEBUG>> Copied ${DOCKER_BACKUP_DIR}/.docker/config.json $(cat ${HOME}/.docker/config.json)"
      fi
    fi

    if [ ! -f "${HOME}/.docker/config.json" ]; then
      /buildeng/get-secret docker/config.json > ${HOME}/.docker/config.json
      echo "DEBUG>> Use /buildeng/get-secret docker/config.json $(cat ${HOME}/.docker/config.json)"
    fi

    echo "DEBUG>> $(ls -l ${HOME}/.docker)"
}

function restore_docker_pem {
    if [ ! -d "${DOCKER_BACKUP_DIR}" ]; then
      return 0
    fi
    rm -rf ${HOME}/.docker
    mv ${DOCKER_BACKUP_DIR} ${HOME}/.docker
    echo "DEBUG>> $(ls -l ${HOME}/.docker)"
}

function debug {
  echo "DEBUG>> AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}"
  echo "DEBUG>> AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}"
  echo "DEBUG>> MAGNOLIA_REPOSITORY_NAME=${MAGNOLIA_REPOSITORY_NAME}"
  echo "DEBUG>> MAGNOLIA_TARGET_NODE=${MAGNOLIA_TARGET_NODE}"
}
debug

mkdir -p ${WORKING_TEMP_FOLDER}/opt/magnolia/repositories/

copy_docker_pem

# DOCKER_HOST may not be defined:
export DOCKER_HOST_BACKUP=${DOCKER_HOST}
export DOCKER_HOST=tcp://${MAGNOLIA_TARGET_NODE}:2376 DOCKER_TLS_VERIFY=1

echo "Step 1 of ${STEP}: Making a copy of the ${SRC_REPO_CONTAINER} daemon repositories ... \
WARNING: short disruption of ${SRC_CONTAINER} service ..."

## WARNING: AUTHOR, PROOF and TRUTH may fight for the stop/start. They should not starts at the same time!!
docker stop ${SRC_CONTAINER}

echo "DEBUG>> should '${SRC_REPO_CONTAINER}' be '${SRC_CONTAINER}' the container name that we stop and start?"
docker cp ${SRC_REPO_CONTAINER}:/opt/magnolia/repositories/. ${WORKING_TEMP_FOLDER}/opt/magnolia/repositories/

docker start ${SRC_CONTAINER}

echo "DEBUG >> $(ls -l  ${WORKING_TEMP_FOLDER}/opt/magnolia/repositories)"

# At this point, we're done interacting with the remote
# docker daemon. Unset DOCKER_TLS_VERIFY
# and restore the original value of DOCKER_HOST
# to interface with the local docker daemon

export DOCKER_HOST=${DOCKER_HOST_BACKUP}
unset DOCKER_TLS_VERIFY

echo "Step 2 of ${STEP}: Creating a docker backup container called ${BACKUP_REPO_CONTAINER} with \
the current ${SRC_REPO_CONTAINER} demon repositories ..."

docker create --name ${BACKUP_REPO_CONTAINER} ubuntu /bin/true
echo "DEBUG>> $(docker container ls -all)"

docker cp ${WORKING_TEMP_FOLDER}/opt/. ${BACKUP_REPO_CONTAINER}:/opt/

echo "Step 3 of ${STEP}: Create a backup image called ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME} using \
the changes in the ${BACKUP_REPO_CONTAINER} ..."

docker commit ${BACKUP_REPO_CONTAINER} ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME}

echo "DEBUG >> $(docker images)"

echo "Step 4 of ${STEP}: Create a tag of the lastest that refers to the backup version ..."

docker tag ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME} ${ABS_LATEST_MAGNOLIA_REPOSITORY_NAME}

echo "DEBUG >> $(docker images)"

echo "Step 5 of ${STEP}: Pushing ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME} to the registry ..."

docker push ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME}

echo "Step 6 of ${STEP}: Pushing ${ABS_LATEST_MAGNOLIA_REPOSITORY_NAME} to the registry ..."

docker push ${ABS_LATEST_MAGNOLIA_REPOSITORY_NAME}


# Cleanup
echo "Step 7 of ${STEP}: cleanup ..."

restore_role
restore_docker_pem

echo "Done. In the Bamboo run environment the cleanup is automatic? How about docker cleanup?"

function cleanup {
  docker rm ${BACKUP_REPO_CONTAINER}
  echo "DEBUG>> $(docker container ls -all)"
  docker rmi ${ABS_BACKUP_MAGNOLIA_REPOSITORY_NAME}
  docker rmi ${ABS_MAGNOLIA_REPOSITORY_NAME}
  echo "DEBUG >> $(docker images)"

  rm -rf ${WORKING_TEMP_FOLDER}
}
#cleanup

